package com.devu.weatherapp.communication;

import java.util.Map;

import io.reactivex.Single;

/**
 * Created by Dev on 22-08-2018.
 */

interface RemoteSource {
    Single getWeatherData(String city);
    Single getGpsWeatherData(double lat,double log);
}
