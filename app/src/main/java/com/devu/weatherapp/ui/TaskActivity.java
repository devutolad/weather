package com.devu.weatherapp.ui;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.devu.weatherapp.R;
import com.devu.weatherapp.ui.base.BaseActivity;

import butterknife.BindView;

/**
 * Created by Dev on 22-08-2018.
 */

public class TaskActivity extends BaseActivity {
    @BindView(R.id.user_input)Button userInput;
    @BindView(R.id.gps_input)Button gpsInput;
    @BindView(R.id.toolbar)Toolbar toolbar;
    @Override
    protected void initializeDagger() {
        setSupportActionBar(toolbar);
        userInput.setOnClickListener(v -> {
            finish();
            startActivity(new Intent(TaskActivity.this, MainActivity.class));
        });
        gpsInput.setOnClickListener(v -> {
            startActivity(new Intent(TaskActivity.this, GPSLocationActivity.class));
            finish();
        });
    }

    @Override
    protected void initializePresenter() {

    }

    @Override
    public int getLayoutid() {
        return R.layout.task_layout;
    }
}
