package com.devu.weatherapp.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.devu.weatherapp.ui.fragments.ForecastFragment;

/**
 * Created by Dev on 22-08-2018.
 */

public class ForecastAdapter extends FragmentStatePagerAdapter {
    public ForecastAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return ForecastFragment.newInstance();
    }

    @Override
    public int getCount() {
        return 1;
    }
}
