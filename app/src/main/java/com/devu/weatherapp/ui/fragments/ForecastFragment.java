package com.devu.weatherapp.ui.fragments;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.devu.weatherapp.R;
import com.devu.weatherapp.ui.adapters.DaysForecastAdapter;
import com.devu.weatherapp.ui.base.BaseFragment;
import com.devu.weatherapp.util.App;

import butterknife.BindView;

/**
 * Created by dev on 22-08-2018.
 */

public class ForecastFragment extends BaseFragment {
    @BindView(R.id.recycler_view_fragment)
    RecyclerView recyclerView;
    @Override
    protected void initializeDagger() {
        App app = (App) getActivity().getApplicationContext();
        app.getMainComponent().inject(ForecastFragment.this);
    }

    @Override
    protected void initializePresenter() {

    }

    @Override
    protected void initializeActivity() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(new DaysForecastAdapter(getActivity()));
    }

    @Override
    public int getLayoutId() {
        return R.layout.forecast_recyclerview;
    }
    public static ForecastFragment newInstance(){
        ForecastFragment forecastFragment=new ForecastFragment();
        return forecastFragment;
    }
}
