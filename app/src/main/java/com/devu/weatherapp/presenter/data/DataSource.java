package com.devu.weatherapp.presenter.data;

import java.util.Map;

import io.reactivex.Single;

/**
 * Created by Dev on 22-08-2018.
 */

public interface DataSource {

Single onWeatherApiCall(String city);
Single onGpsWeatherApiCall(double lat, double log);

}
