package com.devu.weatherapp.presenter.base;

import java.util.Map;

/**
 * Created by Dev on 22-08-2018.
 */

public interface Usecase {

    void getWeatherData(BaseCallback baseCallback, String type);
    void getGpsWeatherData(BaseCallback baseCallback, double lat, double log);
}
