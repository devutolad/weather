
package com.devu.weatherapp.presenter.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Dev on 22-08-2018.
 */

public class WeatherResponseModel {

    @SerializedName("list")
    @Expose
    private java.util.List<DaysList> list = null;
    @SerializedName("city")
    @Expose
    private City city;

    public java.util.List<DaysList> getList() {
        return list;
    }

    public void setList(java.util.List<DaysList> list) {
        this.list = list;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

}
