package com.devu.weatherapp.presenter.interfaces;


import com.devu.weatherapp.presenter.base.BaseView;

/**
 * Created by Dev on 22-08-2018.
 */

public interface WeatherContract {

    interface View<M> extends BaseView {
        void onWeatherSuccess(M model);
        void onFail();
    }
}
