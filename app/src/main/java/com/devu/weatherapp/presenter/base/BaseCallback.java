package com.devu.weatherapp.presenter.base;

/**
 * Created by Dev on 22-08-2018.
 */

public interface BaseCallback <M>{
    void onWeatherSuccess(M model);

    void onFail();
}
