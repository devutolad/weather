package com.devu.weatherapp.presenter.interfaces;

import java.util.Map;

/**
 * Created by Dev on 22-08-2018.
 */

public interface WeatherCallback {
   void onWeatherData(String city);
   void onGpsWeatherData(double lat, double log);
   void unSubscribe();
}
