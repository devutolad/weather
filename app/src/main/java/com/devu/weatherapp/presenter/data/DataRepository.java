package com.devu.weatherapp.presenter.data;

import android.util.Log;

import com.devu.weatherapp.communication.RemoteRepository;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Created by Dev on 22-08-2018.
 */

public class DataRepository implements DataSource {

    private RemoteRepository remoteRepository;
    @Inject
    public DataRepository(RemoteRepository remoteRepository){
        this.remoteRepository=remoteRepository;
    }
    @Override
    public Single onWeatherApiCall(String city) {
        return remoteRepository.getWeatherData(city);
    }

    @Override
    public Single onGpsWeatherApiCall(double lat, double log) {
        return remoteRepository.getGpsWeatherData(lat, log);
    }


}
