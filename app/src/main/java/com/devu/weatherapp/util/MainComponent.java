package com.devu.weatherapp.util;

import com.devu.weatherapp.ui.GPSLocationActivity;
import com.devu.weatherapp.ui.MainActivity;
import com.devu.weatherapp.ui.fragments.ForecastFragment;

import javax.inject.Singleton;

import dagger.Component;


/**
 * Created by Dev on 22-08-2018.
 */

@Singleton
@Component (modules = MainModule.class)
public interface MainComponent {
    void inject(MainActivity mainActivity);
    void inject(ForecastFragment forecastFragment);
    void inject(GPSLocationActivity gpsLocationActivity);

}
