package com.devu.weatherapp.util;

/**
 * Created by Dev on 22-08-2018.
 */

public class Constants {
    public static final String BASE_URL="http://api.openweathermap.org/data/2.5/";
    public static final String API_KEY="4d5798fd1c7a8d8b600e70f7e2a49854";
    public static final int FORECAST_DAY_COUNT = 14;
    public static int ERROR_UNDEFINED = -1;
    public static final String PERMISSION_STATUS = "pref.permission.status";

    // dev key
    //4d5798fd1c7a8d8b600e70f7e2a49854
    // k key
    // 6cd8156d5f8ed21dfca3dbabfb9546bf
}
