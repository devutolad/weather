package com.devu.weatherapp.util;

import com.devu.weatherapp.presenter.model.WeatherResponseModel;


/**
 * Created by Dev on 22-08-2018.
 */

public class LargeDataHandler {
    public static LargeDataHandler largeDataHandler;
    WeatherResponseModel forecastList;
    public static synchronized LargeDataHandler getInstance() {
        if (largeDataHandler == null) {
            largeDataHandler = new LargeDataHandler();
        }
        return largeDataHandler;
    }

    public WeatherResponseModel getForecastList() {
        return forecastList;
    }

    public void setForecastList(WeatherResponseModel forecastList) {
        this.forecastList = forecastList;
    }
    public void clearWeatherData()
    {
        if(forecastList!=null)
        {
            forecastList=null;
        }
    }
}
